#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - create xdg-runtime-dir
# - update image to latest packages
# - install dependencies that are likely needed in one or several projects
# - remove all cached packages to reduce the image size

mkdir -p $XDG_RUNTIME_DIR

# Notes:
# - This is a very minimal hand-picked set of packages.
# - Such that KDE Frameworks, Wrapland and up to KWinFT can be built with it.
zypper install -y -t pattern devel_C_C++
zypper install --no-confirm qt6-base-common-devel git python ms-gsl-devel \
qt6-uitools-devel qt6-multimedia-devel qt6-quickcontrols2-devel qt6-linguist-devel \
qt6-gui-private-devel qt6-wayland qt6-waylandclient-devel qt6-uitools-devel qt6-quickwidgets-devel \
qt6-test-devel qt6-sensors-devel qt6-svg-devel qt6-sql-devel qt6-concurrent-devel qt6-texttospeech-devel \
qt6-shadertools-devel qt6-qt5compat-devel qt6-printsupport-devel qt6-waylandclient-private-devel \
gperf libcap-progs libepoxy-devel libcap-devel libICE-devel libSM-devel libboost_headers-devel \
libXcursor-devel xcb-util-cursor-devel xcb-util-devel xcb-util-wm-devel xcb-util-image-devel bzip2 \
xcb-util-keysyms-devel xwayland-devel wayland-protocols-devel libpixman-1-0-devel libgbm-devel \
libcap-progs polkit-devel libgcrypt-devel pam-devel libcanberra-devel libzstd-devel xz-devel \
pipewire-devel ffmpeg-6-libavcodec-devel ffmpeg-6-libavformat-devel ffmpeg-6-libavfilter-devel libva-devel

# Plan:
# - clone FDBuild
# - build all KDE Frameworks
# - cleanup tooling and build files

python3 -m venv /opt/venv
source /opt/venv/bin/activate
pip3 install fdbuild

mkdir -p /opt/kde-build
cd /opt/kde-build

printf "build:
  plugin: ninja
  threads: max
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
    - QT_MAJOR_VERSION=6
    - BUILD_WITH_QT6=ON
    - BUILD_QT5=OFF
install:
  path: /usr
source:
  plugin: git
  depth: 1
structure:
  enabled: true
  plugin: kde
  branch-group: kf6-qt6
  flat: true
  selection:
  - kwin\n" > fdbuild.yaml

fdbuild --noconfirm --only-structure

# Disalbe kapidox. It does not contain a valid CMake file and build fails.
# TODO(romangg): remove once FDBuild handles this correctly.
sed -i 's/^- kapidox/#- kapidox/g' fdbuild.yaml
sed -i 's/^- kdoctools/#- kdoctools/g' fdbuild.yaml
sed -i 's/^- kwin$/#- kwin/g' fdbuild.yaml
sed -i 's/enabled\: true/enabled\: false/g' fdbuild.yaml

fdbuild --noconfirm

# venv deactivate
deactivate

cd /opt
rm -rf /opt/kde-build
rm -rf /opt/venv
