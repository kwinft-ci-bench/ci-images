#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - clone FDBuild
# - build all KDE Frameworks
# - cleanup tooling and build files

python -m venv /opt/venv
source /opt/venv/bin/activate
pip install fdbuild

mkdir -p /opt/kde-build
cd /opt/kde-build

printf "install:
  path: /usr
configure:
  plugin: cmake
  options:
    - BUILD_TESTING=OFF
    - QT_MAJOR_VERSION=6
    - BUILD_WITH_QT6=ON
    - PHONON_BUILD_QT5=OFF
build:
  plugin: ninja
  threads: max
source:
  plugin: git
  depth: 1
structure:
  enabled: true
  plugin: kde
  branch-group: kf6-qt6
  flat: true
  selection:
  - frameworks\n" > fdbuild.yaml

fdbuild --noconfirm --only-structure

# Disalbe kapidox. It does not contain a valid CMake file and build fails.
# TODO(romangg): remove once FDBuild handles this correctly.
sed -i 's/- kapidox/#- kapidox/g' fdbuild.yaml
sed -i 's/enabled\: true/enabled\: false/g' fdbuild.yaml

fdbuild --noconfirm

# venv deactivate
deactivate

cd /opt
rm -rf /opt/kde-build
rm -rf /opt/venv
