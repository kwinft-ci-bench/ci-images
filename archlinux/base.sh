#!/bin/bash

set -ex
set -o pipefail

# Plan:
# - create xdg-runtime-dir
# - update image to latest packages
# - install dependencies that are likely needed in one or several projects
# - remove all cached packages to reduce the image size

mkdir -p $XDG_RUNTIME_DIR

pacman -Syu --quiet --noconfirm

# Notes:
# - This is a very minimal hand-picked set of packages.
# - Such that KDE Frameworks, Wrapland and up to KWinFT can be built with it.
# - Fat Boost lib in there is annoying to a certain degree. First time needed by KActivities.
pacman -S --needed --quiet --noconfirm \
base-devel git pkgconf clang llvm meson ninja cmake doxygen microsoft-gsl \
qt6-base qt6-svg qt6-wayland qt6-multimedia qt6-tools qt6-declarative \
qt6-virtualkeyboard qca-qt6 qt6-sensors qt6-shadertools qt6-speech \
upower networkmanager giflib shared-mime-info gperf boost lmdb libcanberra \
qrencode libical docbook-xsl perl-uri polkit pipewire \
xorg-server xorg-server-xvfb xorg-server-devel wayland-protocols \
xcursor-vanilla-dmz xcb-util-cursor xorg-server-xwayland mesa-demos mesa-utils \
python python-yaml python-lxml python-pip python-pygments \
valgrind stress go wlroots

pacman -Scc --noconfirm
